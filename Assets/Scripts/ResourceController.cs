using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ResourceController : MonoBehaviour
{
    int _index;
    int _level {
        set {
            //Save the value set to _level at progress data
            UserDataManager.Progress.ResourcesLevels[_index] = value;
            UserDataManager.Save();
        }

        get {
            //Check whether index is already on the progress data
            if (!UserDataManager.HasResources(_index)) {
                //show level 1
                return 1;
            }

            return UserDataManager.Progress.ResourcesLevels[_index];
        }
    }

    public Button ResourceButton;
    public Image ResourceImage;
    public Text ResourceDescription;
    public Text ResourceUpgradeCost;
    public Text ResourceUnlockCost;

    ResourceConfig _config;

    public bool IsUnlocked {get; private set;}

    void Start() {
        ResourceButton.onClick.AddListener(() => {
            if (IsUnlocked) {
                UpgradeLevel();
            } else {
                UnlockResource();
            }
        });
    }
    
    public void SetConfig (int index, ResourceConfig config) {
        _index = index;
        _config = config;

        ResourceDescription.text = $"{_config.Name} Lv. {_level}\n+{GetOutput().ToString("0")}";
        ResourceUnlockCost.text = $"Unlock Cost\n{_config.UnlockCost}";
        ResourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";

        SetUnlocked(_config.UnlockCost == 0 || UserDataManager.HasResources(_index));
    }

    public double GetOutput() {
        return _config.Output*_level;
    }

    public double GetUpgradeCost() {
        return _config.UpgradeCost*_level;
    }

    public double GetUnlockCost() {
        return _config.UnlockCost;
    }

    public void UpgradeLevel() {
        double upgradeCost = GetUpgradeCost();
        EventSystem.current.SetSelectedGameObject(null);
        if (UserDataManager.Progress.Gold < upgradeCost) {
            return;
        }

        GameManager.Instance.AddGold(-upgradeCost);
        _level++;

        ResourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";
        ResourceDescription.text = $"{_config.Name} Lv. {_level}\n+{GetOutput().ToString("0")}";
    }

    public void UnlockResource() {
        double unlockCost = GetUnlockCost();
        EventSystem.current.SetSelectedGameObject(null);
        if (UserDataManager.Progress.Gold < unlockCost) {
            return;
        }

        SetUnlocked(true);
        GameManager.Instance.ShowNextResource();

        AchievementController.Instance.UnlockAchievement(AchievementType.UnlockResource, _config.Name);
    }

    public void SetUnlocked(bool unlocked){
        IsUnlocked = unlocked;
        ResourceUnlockCost.gameObject.SetActive(!unlocked);
        ResourceUpgradeCost.gameObject.SetActive(unlocked);

        if (unlocked) {
            if (!UserDataManager.HasResources(_index)) {
                UserDataManager.Progress.ResourcesLevels.Add(_level);
                UserDataManager.Save();
            }
        }

        // ResourceImage.color = IsUnlocked ? Color.white : Color.grey;
        // ResourceUnlockCost.gameObject.SetActive(!unlocked);
        // ResourceUpgradeCost.gameObject.SetActive(unlocked);
    } 
}
