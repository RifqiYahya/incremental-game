using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    static GameManager _instance = null;
    public static GameManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    [Range(0f, 1f)]
    public float AutoCollectPercentage = 0.1f;
    public ResourceConfig[] ResourcesConfigs;
    public Sprite[] ResourcesSprites;

    public Transform ResourceParent;
    public ResourceController ResourcePrefab;
    public TapText TapTextPrefab;

    public Transform CoinIcon;
    public Text GoldInfo;
    public Text AutoCollectInfo;

    List<ResourceController> _activeResources = new List<ResourceController>();
    List<TapText> _tapTextPool = new List<TapText>();
    float _collectSecond =0f;

    Camera _camera;

    // Start is called before the first frame update
    void Start()
    {
        _camera = FindObjectOfType<Camera>();
        AddAllResources();
        CoinIcon.transform.position = _camera.ScreenToWorldPoint(new Vector3(Screen.width * 0.3f, Screen.height/2, 10f)); 
    }

    // Update is called once per frame
    void Update()
    {
        _collectSecond += Time.unscaledDeltaTime;
        if (_collectSecond > 1f) {
            CollectPerSecond();
            _collectSecond=0f;
        }

        CoinIcon.transform.localScale = Vector3.LerpUnclamped(CoinIcon.transform.localScale, Vector3.one * 2f, 0.15f);

        CoinIcon.transform.Rotate(0f, 0f,Time.deltaTime*-100f);

        CheckResourceCost();
    }

    void CheckResourceCost() {
        foreach(ResourceController resource in _activeResources) {
            bool isBuyable = false;

            if(resource.IsUnlocked) {
                isBuyable = UserDataManager.Progress.Gold >= resource.GetUpgradeCost();
            } else {
                isBuyable = UserDataManager.Progress.Gold >= resource.GetUnlockCost();
            }

            resource.ResourceImage.color = UserDataManager.Progress.Gold>resource.GetUnlockCost() ? Color.white : Color.grey;

            resource.ResourceImage.sprite = ResourcesSprites[isBuyable ? 1 : 0];
            resource.ResourceButton.gameObject.SetActive(isBuyable);
        }
    }

    void AddAllResources() {
        bool showResources = true;
        int index = 0;

        foreach(ResourceConfig config in ResourcesConfigs) {
            GameObject obj = Instantiate(ResourcePrefab.gameObject, ResourceParent, false);
            ResourceController resource = obj.GetComponent<ResourceController>();

            resource.SetConfig(index, config);

            obj.gameObject.SetActive(showResources);

            if (showResources && !resource.IsUnlocked) {
                showResources = false;
            }

            _activeResources.Add(resource);
            index++;
        }
    }

    public void ShowNextResource() {
        foreach(ResourceController resource in _activeResources) {
            if(!resource.gameObject.activeSelf) {
                resource.gameObject.SetActive(true);
                break;
            }
        }
    }

    void CollectPerSecond() {
        double output = 0;
        foreach (ResourceController resource in _activeResources) {
            if (resource.IsUnlocked){
                output += resource.GetOutput();
            }
        }

        output *= AutoCollectPercentage;
        AutoCollectInfo.text = $"Auto collect : {output.ToString("N1")}/second";
        AddGold(output);
    }

    public void AddGold(double value) {
        UserDataManager.Progress.Gold += value;
        GoldInfo.text = $"Gold:{UserDataManager.Progress.Gold.ToString("0")}";
    }

    public void CollectByTap(Vector3 tapPosition, Transform parent) {
        double output = 0;
        foreach (ResourceController resource in _activeResources) {
            if(resource.IsUnlocked){
                output += resource.GetOutput();
            }
        }

        TapText tapText = GetOrCreateTapText();
        tapText.transform.SetParent(parent, false);
        tapText.transform.position = tapPosition;

        tapText.Text.text = $"+{output.ToString("0")}";
        tapText.gameObject.SetActive(true);
        CoinIcon.transform.localScale = Vector3.one*1.75f;

        AddGold(output);
    }

    TapText GetOrCreateTapText() {
        TapText tapText = _tapTextPool.Find(t => !t.gameObject.activeSelf);
        if (tapText == null) {
            tapText = Instantiate(TapTextPrefab).GetComponent<TapText>();
            _tapTextPool.Add(tapText);
        }

        return tapText;
    }
}

[System.Serializable]
public struct ResourceConfig {
    public string Name;
    public double UnlockCost;
    public double UpgradeCost;
    public double Output;
}
