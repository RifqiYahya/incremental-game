using UnityEngine;

public class UserDataManager
{
    private const string PROGRESS_KEY = "Progress";

    public static UserProgressData Progress;

    public static void Load() {
        //Check wheterh there is a data stored as PROGRESS_KEY

        if (!PlayerPrefs.HasKey(PROGRESS_KEY)) {
            //If not exist
            Progress = new UserProgressData();

            Save();
        } else {
            // If exist overwrite the data
            string json = PlayerPrefs.GetString(PROGRESS_KEY);
            Progress = JsonUtility.FromJson<UserProgressData>(json);
        }
    }

    public static void Save() {
        string json = JsonUtility.ToJson(Progress);
        PlayerPrefs.SetString(PROGRESS_KEY, json);
    }

    public static bool HasResources(int index) {
        
        for (int i = 0; i < Progress.ResourcesLevels.Count; i ++) {
            if(i == index) {
                return true;
            }
        }

        return false;
        
    }
}
